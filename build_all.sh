#!/bin/bash

# Usage: ./build_all.sh [name]

# builds a cross compile toolchain for building linux(uclibc-nc) for the arm cortex r5
# provide a unique name to build in a different build folder, default name: "default"

set -e

SCRIPT_PATH="$(dirname "$(realpath -s "$0")")"

name="${1:-default}"
builddir="$(pwd)/build/$name"

export PATH="$builddir/output/bin:$PATH"

mkdir -p "$builddir/logs"

echo "Downloading & Extracting ..."
"$SCRIPT_PATH/support/download.sh" "$@" &> "$builddir/logs/download.log"

echo "Helper symlinks ..."
"$SCRIPT_PATH/support/symlinks.sh" "$@" &> "$builddir/logs/symlinks.log"

echo "Linux Headers ..."
"$SCRIPT_PATH/support/linux_headers.sh" "$@" &> "$builddir/logs/linux_headers.log"

echo "GCC Part 1/3 ..."
"$SCRIPT_PATH/support/gcc1.sh" "$@" &> "$builddir/logs/gcc1.log"

echo "uClibc-ng Part 1/2 ..."
"$SCRIPT_PATH/support/libc1.sh" "$@" &> "$builddir/logs/libc1.log"

echo "GCC Part 2/3 ..."
"$SCRIPT_PATH/support/gcc2.sh" "$@" &> "$builddir/logs/gcc2.log"

echo "uClibc-ng Part 2/2 ..."
"$SCRIPT_PATH/support/libc2.sh" "$@" &> "$builddir/logs/libc2.log"

echo "GCC Part 3/3 ..."
"$SCRIPT_PATH/support/gcc3.sh" "$@" &> "$builddir/logs/gcc3.log"

echo "Done, it worked!"
