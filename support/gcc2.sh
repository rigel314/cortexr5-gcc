#!/bin/bash

set -e

SCRIPT_PATH="$(dirname "$(realpath -s "$0")")"

name="${1:-default}"

builddir="$(pwd)/build/$name"

# build second part of gcc
cd "$builddir/src/gcc-build"
make -j$(nproc) -l$(nproc) all-target-libgcc
make -j$(nproc) -l$(nproc) install-target-libgcc
