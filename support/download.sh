#!/bin/bash

set -e

name="${1:-default}"

builddir="$(pwd)/build/$name"

mkdir -p "$builddir/downloads"
mkdir -p "$builddir/src"
mkdir -p "$builddir/output/arm-uclinuxfdpiceabi"
mkdir -p "$builddir/output/bin"

curl -fsSL -o "$builddir"/downloads/uclibc-ng.tar.lz https://downloads.uclibc-ng.org/releases/1.0.49/uClibc-ng-1.0.49.tar.xz
curl -fsSL -o "$builddir"/downloads/gcc.tar.lz https://mirrors.kernel.org/gnu/gcc/gcc-13.2.0/gcc-13.2.0.tar.xz
curl -fsSL -o "$builddir"/downloads/binutils.tar.lz https://gnu.mirror.constant.com/binutils/binutils-2.42.tar.lz
git clone -b xilinx-v2024.1 --depth=1 https://github.com/Xilinx/linux-xlnx.git "$builddir"/src/linux

tar xf "$builddir"/downloads/uclibc-ng.tar.lz -C "$builddir"/src/
mv "$builddir"/src/uClibc-ng* "$builddir"/src/uclibc-ng
tar xf "$builddir"/downloads/gcc.tar.lz -C "$builddir"/src/
mv "$builddir"/src/gcc* "$builddir"/src/gcc
tar xf "$builddir"/downloads/binutils.tar.lz -C "$builddir"/src/
mv "$builddir"/src/binutils* "$builddir"/src/binutils

mkdir -p "$builddir/src/gcc-build"
