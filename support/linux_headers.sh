#!/bin/bash

set -e

SCRIPT_PATH="$(dirname "$(realpath -s "$0")")"

name="${1:-default}"

builddir="$(pwd)/build/$name"

# install linux headers
cd "$builddir/src/linux"
cp "$SCRIPT_PATH"/linux.config .config
make ARCH=arm INSTALL_HDR_PATH="$builddir/output/arm-uclinuxfdpiceabi" headers_install
