#!/bin/bash

set -e

SCRIPT_PATH="$(dirname "$(realpath -s "$0")")"

name="${1:-default}"

builddir="$(pwd)/build/$name"

mv "$builddir/src/gcc/libiberty" "$builddir/src/gcc/libiberty.gcc"

# make symlinks in the gcc sources to all of the folders in binutils
for i in $(find "$builddir/src/binutils" -maxdepth 1 -mindepth 1 -type d); do
    filename="${i#$builddir/src/binutils}"
    relpath=$(realpath --relative-to="$builddir/src/gcc" "$i")
    if [[ ! -e "$builddir/src/gcc/$filename" ]]; then
        ln -sfn $i "$builddir/src/gcc/$filename"
    fi
done

# patch
cp support/tsystem.h "$builddir/src/gcc/gcc/"
cp "$builddir/src/binutils/include/demangle.h" "$builddir/src/gcc/include/"

# configure and build first part of gcc
cd "$builddir/src/gcc-build"
../gcc/configure --prefix="$builddir/output/" --target=arm-uclinuxfdpiceabi --enable-languages=c,c++ --with-arch=armv7-r --with-tune=cortex-r5 --with-fpu=vfpv3 --enable-fdpic
make -j$(nproc) -l$(nproc) all-gcc
make -j$(nproc) -l$(nproc) install-gcc
make -j$(nproc) -l$(nproc) install-binutils
make -j$(nproc) -l$(nproc) install-ld
