#!/bin/bash

set -e

SCRIPT_PATH="$(dirname "$(realpath -s "$0")")"

name="${1:-default}"

builddir="$(pwd)/build/$name"

# Add a symlink that should make stuff work
ln -sf arm-uclinuxfdpiceabi-as "$builddir/output/bin/as"

# build the real libc
cd "$builddir/src/uclibc-ng"
CROSS_COMPILE=arm-uclinuxfdpiceabi- make -j$(nproc) -l$(nproc)
CROSS_COMPILE=arm-uclinuxfdpiceabi- make -j$(nproc) -l$(nproc) install

rm "$builddir/output/bin/as"
