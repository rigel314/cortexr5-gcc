#!/bin/bash

set -e

SCRIPT_PATH="$(dirname "$(realpath -s "$0")")"

name="${1:-default}"

builddir="$(pwd)/build/$name"

# create symlinks to make things work
ln -sfn . "$builddir/output/arm-uclinuxfdpiceabi/usr"
ln -sf ../../src/gcc-build/gas/as-new "$builddir/output/bin/arm-uclinuxfdpiceabi-as"
# ln -sf arm-uclinuxfdpiceabi-as "$builddir/output/bin/as"
