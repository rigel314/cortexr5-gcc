#!/bin/bash

set -e

SCRIPT_PATH="$(dirname "$(realpath -s "$0")")"

name="${1:-default}"

builddir="$(pwd)/build/$name"

# Add a symlink that should make stuff work
ln -sf arm-uclinuxfdpiceabi-as "$builddir/output/bin/as"

# patch
cp support/dl-vdso.c "$builddir/src/uclibc-ng/ldso/ldso/"

# build the startfiles and headers and make a dummy libc
cd "$builddir/src/uclibc-ng"
sed -re "s#__LOCALPATH__#$builddir#" "$SCRIPT_PATH"/uclibc-ng.config > .config
CROSS_COMPILE=arm-uclinuxfdpiceabi- make -j$(nproc) -l$(nproc) install_headers
CROSS_COMPILE=arm-uclinuxfdpiceabi- make -j$(nproc) -l$(nproc) install_startfiles
arm-uclinuxfdpiceabi-gcc -nostdlib -nostartfiles -shared -x c /dev/null -o "$builddir/output/arm-uclinuxfdpiceabi/lib/libc.so"

rm "$builddir/output/bin/as"
